import React, {useEffect, useState} from "react";
import Box from "./box";
import DecodeFile from './decode.json'
import EncodeFile from './encode.json'

const original = 'original'
const encoded = 'encoded'

export default function Container(props){
    const [duty,setDuty] = useState('')
    const [content,setContent] = useState('')
    const [result,setResult] = useState('')

    function update(content,duty) {
        setDuty(duty)
        setContent(content)
    }

    useEffect(()=>{
        const t = calculate(duty,content)
        if (t!=null) setResult(t)
    },[content,duty])

    return <div>
        <Box duty={original} mode={duty} content={content} result={result} update={update}/>
        <Box duty={encoded} mode={duty} content={content} result={result} update={update}/>
    </div>
}

function calculate(duty, content) {
    switch (duty) {
        case original:
            return encode(content)
        case encoded:
            return decode(content)
        default:
            return null
    }
}

function decode(content) {
    console.log(content)
    let ret = ""
    for (const i of content){
        const matched = DecodeFile[i]
        console.log(i,matched)
        if (matched!=null){
            ret=ret.concat(matched)
        }
    }
    try {
        return atob(decodeURIComponent(ret))
    }catch (e) {
        return "error"
    }

}

function encode(content) {
    content = btoa(unescape(encodeURIComponent(content)))
    let ret = ""
    for (const i of content){
        const matched = EncodeFile[i]
        if (matched!=null){
            ret=ret.concat(matched)
        }
    }
    return ret
}


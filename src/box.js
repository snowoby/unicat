import React from "react";

export default function Box(props) {
    return <div>
        {props.duty}
        <textarea value={props.mode===props.duty?props.content:props.result} onChange={e => props.update(e.target.value,props.duty)}/>
    </div>
}